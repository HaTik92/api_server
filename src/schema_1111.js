const { gql } = require('apollo-server-express');

const typeDefs = gql`
    type Product {
        id: Int!
        photo:  String
        code:  String
        name:  String
        price: Float
        salePrice: Float
        rating: Float
        status: [Status]
        weight:  Float
        dimensions: [Dimension]
        categories: [Category]
        description: [Description]
        photos: String
        review: Float
        sm_pictures: String
        slug: String
        new: String    
    }
    type Status {
            title: String
            value: String
        }
    type Dimension {
        length: Float
        width: Float
        height: Float
    }

    type Category {
        id: Int!
        name: String
        slug: ID
    }
    type Description {
        location: [Location]
        material: [Material]
        color:[Color]
        size: [Size]
    }
    type Location {
        id: Int!
        name: String
    }
    type Material {
        id: Int!
        name: String
    }
    type Color {
        id: Int!
        name: String
    }
    type Size {
        id: Int!
        name: String
    }
    
type Media {
        width: Int
        height: Int
        url: String
    }
    type Post {
        id: Int!
        author: String
        comments: Int
        content: String
        date: String
        slug: ID
        title: String
        type: PostType
        blog_categories: [Category]
        image: [Media]
    }

    enum PostType {
        image
        video
        gallery
    }

    type ProductSingle {
        single: Product
        prev: Product
        next: Product
        related: [Product]
    }

    type ShopResponse {
        data: [Product],
        totalCount: Int
    }

    type BlogPageResponse {
        data: [Post]
        categories: [BlogCategory]
    }

    type BlogCategory {
        name: String,
        slug: String,
        count: Int
    }

    type PostSingle {
        single: Post,
        prev: Post
        next: Post
        related: [Post]
        categories: [BlogCategory]
    }

    type homeResponse {
        products: [Product],
        posts: [Post]
    }

    type Query {
        elementProducts: [Product]
        elementPosts: [Post]
        homeData(demo: Int): homeResponse
        products(demo: Int!, searchTerm: String, color: [String] = [], size: [String] = [], brand: [String] = [], minPrice: Int = 0, maxPrice: Int = 150000, category: String, rating: [String], sortBy: String, page: Int = 1, perPage: Int = 10, list: Boolean, from: Int = 0 ): ShopResponse,
        product(demo: Int!, slug: String!, onlyData: Boolean): ProductSingle
        postsByPage(page: String, category: String): BlogPageResponse
        post(slug: String!): PostSingle
    }
`

module.exports = typeDefs;