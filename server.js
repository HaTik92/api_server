const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./src/schema');
const resolvers = require('./src/resolver');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: true
});

const app = express();
server.applyMiddleware({ app, bodyParserConfig: true });
app.use('/uploads', express.static('src/images'));
app.listen({ port: 4040 }, () =>
    console.log(`🚀 Server ready at http://localhost:4040${server.graphqlPath}`)
);